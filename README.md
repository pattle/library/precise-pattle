# Precise Pattle

[![pub package](https://img.shields.io/pub/v/precise_pattle.svg)](https://pub.dartlang.org/packages/precise_pattle)
[![license: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![style: precise_pattle](https://img.shields.io/badge/style-precise__pattle-AA4139)](https://pub.dartlang.org/packages/precise_pattle)


A set of practices (linter rules) used in [Pattle](https://pattle.dev) projects, such as
[Pattle itself](https://pattle.im), and the
[matrix_sdk](https://pub.dev/packages/matrix_sdk).

This package uses rules of and is inspired by
[effective_dart](https://pub.dev/packages/effective_dart),
[pedantic](https://pub.dev/packages/pedantic),
and [Flutter](https://github.com/flutter/flutter/blob/master/analysis_options.yaml).

## Usage

To use these analysis rules in your own project, add it as a dev dependency
in your `pubspec.yaml`:

```yaml
dev_dependencies:
  precise_pattle: ^1.0.5
```

Then include it in your `analysis_options.yaml`:

```yaml
include: package:precise_pattle/analysis_options.yaml
```

### Further info

More info on how to use the Dart analyzer can be found
[here](https://dart.dev/guides/language/analysis-options).
