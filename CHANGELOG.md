## 1.0.5

- Remove `pedantic` dependency

## 1.0.4

- Format code

## 1.0.3

- Make pub.dev happy
- Fix `README`

## 1.0.2

- Fix `pubspec` links

## 1.0.1

- Fix `effective_dart` and `pedantic` being `dev_dependencies` instead of
  normal `dependencies`

## 1.0.0

- Initial version
